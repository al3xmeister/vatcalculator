﻿using UIKit;
using VatCalculator.Controls;
using VatCalculator.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CenteredEntry), typeof(CenteredEntryRenderer))]
namespace VatCalculator.iOS.Renderers
{
    class CenteredEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;

            Control.LeftViewMode = UITextFieldViewMode.Always;
            Control.TextColor = UIColor.White;
            Control.VerticalAlignment = UIControlContentVerticalAlignment.Center;
            Control.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
        }
    }
}