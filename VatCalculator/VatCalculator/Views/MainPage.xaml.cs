﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace VatCalculator.Views
{
    public partial class MainPage : ContentPage
    {

        int currentState = 1;
        string mathOperator;
        double firstNumber, secondNumber;
        public MainPage()
        {
            InitializeComponent();
            OnClear(this, null);
            VatAmountEntry.TextChanged += OnVATEntryChanged;
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void OnVATEntryChanged(object sender, EventArgs e)
        {
            try
            {
                secondNumber = double.Parse(VatAmountEntry.Text);

                if (!string.IsNullOrWhiteSpace (secondNumber.ToString())) 
                    {
                    mathOperator = "VAT";
                }

            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex);
            }

        }
        protected override async void OnDisappearing()
        {
            base.OnDisappearing();
            VatAmountEntry.TextChanged -= OnVATEntryChanged;
        }

        void OnNumberTapped(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;

            if (resultText.Text == "0" || currentState < 0)
            {
                resultText.Text = "";
                if (currentState < 0)
                    currentState *= -1;
            }

            resultText.Text += pressed;

            double number;
            if (double.TryParse(resultText.Text, out number))
            {
                resultText.Text = number.ToString("N0");
                if (currentState == 1)
                {
                    firstNumber = number;
                }
                else
                {
                    secondNumber = number;
                }
            }
        }

        void OnSelectOperator(object sender, EventArgs e)
        {
            currentState = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            mathOperator = pressed;
        }

        void OnClear(object sender, EventArgs e)
        {
            firstNumber = 0;
            secondNumber = 0;
            currentState = 1;
            resultText.Text = "0";
        }

        void OnCalculateVAT(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(VatAmountEntry.Text))
            {
                secondNumber = double.Parse(VatAmountEntry.Text.ToString());
            }

                var result = Utility.VatCalculator.Calculate(firstNumber, secondNumber, mathOperator);

                resultText.Text = result.ToString();
                firstNumber = result;
                currentState = -1;
        }
        void OnCalculate(object sender, EventArgs e)
        {
            if (currentState == 2)
            {
                var result = Utility.VatCalculator.Calculate(firstNumber, secondNumber, mathOperator);

                resultText.Text = result.ToString();
                firstNumber = result;
                currentState = -1;
            }
        }
    }
}

