﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Java.Util;
using VatCalculator.Controls;
using VatCalculator.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CenteredEntry), typeof(CenteredEntryRenderer))]
namespace VatCalculator.Droid.Renderers
{
#pragma warning disable 618
    public class CenteredEntryRenderer : EntryRenderer
#pragma warning restore 618
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if(Control == null) return;
            this.Control.Gravity = GravityFlags.Center;

        }
    }
}